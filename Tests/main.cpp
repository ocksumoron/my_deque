//
//  main.cpp
//  Tests
//
//  Created by Даня on 06.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#include <iostream>
#include "gtest/gtest.h"

int Factorial( int n );

// Tests factorial of 0.
TEST(FactorialTest, HandlesZeroInput) {
    EXPECT_EQ(1, Factorial(0));
}

// Tests factorial of positive numbers.
TEST(FactorialTest, HandlesPositiveInput) {
    EXPECT_EQ(1, Factorial(1));
    EXPECT_EQ(2, Factorial(2));
    ASSERT_EQ(6, Factorial(3));
    EXPECT_EQ(40320, Factorial(8));
}

int Factorial( int n )
{
    int result = 1;
    for( int i = 2; i <= n; i++ ) {
        result *= i;
    }
    return result;
}


int main(int argc, char * argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
