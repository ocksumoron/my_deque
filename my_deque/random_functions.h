//
//  random_functions.h
//  my_deque
//
//  Created by Даня on 22.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#ifndef my_deque_random_functions_h
#define my_deque_random_functions_h

typedef long long ll;
typedef unsigned int ui;

#include <random>

ui generateRandom(ui minx, ui maxx) {
    static std::default_random_engine generator;
    std::uniform_int_distribution<ui> distribution(minx, maxx);
    return distribution(generator);
}

int generateRandomValue() {
    static std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(INT_MIN, INT_MAX);
    return distribution(generator);
}

#endif
