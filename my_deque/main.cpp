//
//  main.cpp
//  MyDeque
//
//  Created by Даня on 01.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#include "random_functions.h"
#include "deque.h"
#include <deque>
#include <gtest/gtest.h>
#include <cassert>

enum EQuery_Type {
    QPUSH_FRONT,
    QPUSH_BACK,
    QPOP_FRONT,
    QPOP_BACK,
    QEQUAL_METHODS,
    QSIZE
};


class DequeResultTest: public testing::TestWithParam<size_t> {
};

class DequeTimeTest: public testing::TestWithParam<size_t> {
};

class LargeDequeTimeTest: public testing::TestWithParam<size_t> {
};

TEST_P(DequeResultTest, ResultTest) {
    MyDeque<int> myDeque;
    std::deque<int> stlDeque;
    for (ui i = 0; i < GetParam(); ++i) {
        EQuery_Type currentQuery = static_cast<EQuery_Type>(generateRandom(0, QSIZE - 1));
        int queryValue;
        switch (currentQuery) {
            case QPOP_BACK:
                if (!stlDeque.empty()) {
                    myDeque.pop_back();
                    stlDeque.pop_back();
                }
                break;
            case QPUSH_FRONT:
                queryValue = generateRandomValue();
                myDeque.push_front(queryValue);
                stlDeque.push_front(queryValue);
                break;
            case QPUSH_BACK:
                queryValue = generateRandomValue();
                myDeque.push_front(queryValue);
                stlDeque.push_front(queryValue);
                break;
            case QPOP_FRONT:
                if (!stlDeque.empty()) {
                    myDeque.pop_front();
                    stlDeque.pop_front();
                }
                break;
            case QEQUAL_METHODS:
                EXPECT_TRUE(myDeque.size() == stlDeque.size());
                EXPECT_TRUE(myDeque.empty() == stlDeque.empty());
                if (!myDeque.empty()) {
                    EXPECT_TRUE(myDeque.front() == stlDeque.front());
                    EXPECT_TRUE(myDeque.back() == stlDeque.back());
                    queryValue = generateRandom(0, myDeque.size());
                    EXPECT_TRUE(myDeque[queryValue] == stlDeque[queryValue]);
                }
                break;
            default:
                assert(false);
        }
    }
    EXPECT_TRUE(std::equal(myDeque.begin(), myDeque.end(), stlDeque.begin()));
    EXPECT_TRUE(std::equal(myDeque.rbegin(), myDeque.rend(), stlDeque.cbegin()));
    EXPECT_TRUE(std::equal(myDeque.cbegin(), myDeque.cend(), stlDeque.cbegin()));
    EXPECT_TRUE(std::equal(myDeque.crbegin(), myDeque.crend(), stlDeque.crbegin()));
    SUCCEED();
}

TEST_P(DequeTimeTest, TimeTest) {
    MyDeque<int> myDeque;
    for (ui i = 0; i < GetParam(); ++i) {
        EQuery_Type currentQuery = static_cast<EQuery_Type>(generateRandom(0, QSIZE - 2));
        switch (currentQuery) {
            case QPOP_BACK:
                if (!myDeque.empty()) {
                    myDeque.pop_back();
                }
                break;
            case QPUSH_FRONT:
                myDeque.push_front(generateRandomValue());
                break;
            case QPUSH_BACK:
                myDeque.push_back(generateRandomValue());
                break;
            case QPOP_FRONT:
                if (!myDeque.empty()) {
                    myDeque.pop_front();
                }
                break;
            default:
                assert(false);
        }
    }
    SUCCEED();
}

TEST_P(LargeDequeTimeTest, LargeTimeTest) {
    MyDeque<int> myDeque;
    for (ui i = 0; i < GetParam(); ++i) {
        EQuery_Type currentQuery = static_cast<EQuery_Type>(generateRandom(0, QSIZE - 4));
        switch (currentQuery) {
            case QPUSH_BACK:
                myDeque.push_back(generateRandomValue());
                break;
            case QPUSH_FRONT:
                myDeque.push_back(generateRandomValue());
            default:
                assert(false);
        }
    }
    for (ui i = 0; i < GetParam(); ++i) {
        EQuery_Type currentQuery = static_cast<EQuery_Type>(generateRandom(0, QSIZE - 2));
        switch (currentQuery) {
            case QPOP_BACK:
                if (!myDeque.empty()) {
                    myDeque.pop_back();
                }
                break;
            case QPUSH_FRONT:
                myDeque.push_front(generateRandomValue());
                break;
            case QPUSH_BACK:
                myDeque.push_back(generateRandomValue());
                break;
            case QPOP_FRONT:
                if (!myDeque.empty()) {
                    myDeque.pop_front();
                }
                break;
            default:
                assert(false);
        }
    }
    SUCCEED();
}

INSTANTIATE_TEST_CASE_P(TestMyDequeAlgorithm, DequeResultTest, testing::Values(1e0, 1e1, 1e2, 1e3, 1e4));
INSTANTIATE_TEST_CASE_P(TestMyDequeTime, DequeTimeTest, testing::Values(1e0, 1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8));
INSTANTIATE_TEST_CASE_P(TestMyDequeLargeTime, LargeDequeTimeTest, testing::Values(1e0, 1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7));

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return 0;
}
