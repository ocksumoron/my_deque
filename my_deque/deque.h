//
//  deque.h
//  MyDeque
//
//  Created by Даня on 20.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#ifndef MyDeque_deque_h
#define MyDeque_deque_h


#include <gtest/gtest.h>
#include <iostream>
#include <cassert>
#include <algorithm>

typedef unsigned int ui;

enum EOperationType {
    PUSH_BACK,
    POP_BACK,
    PUSH_FRONT,
    POP_FRONT
};

template <typename ElementType>
class MyDeque {
    //    class my_iterator {
    //        ui point;
    //        ui buffer_size;
    //        my_iterator(): point(0) {}
    //        my_iterator(ui point): point(point) {}
    //    public:
    //        void setBufferSize(ui new_buffer_size) {
    //            buffer_size = new_buffer_size;
    //            point %= buffer_size;
    //        }
    //        my_iterator &operator++(int){
    //            point++;
    //            point %= buffer_size;
    //            return *this;
    //        }
    //        my_iterator &operator++ (){
    //            point++;
    //            point %= buffer_size;
    //            return *this;
    //        }
    //        my_iterator &operator--(int) {
    //            point--;
    //            if (point < 0)
    //                point += buffer_size;
    //            return *this;
    //        }
    //        my_iterator &operator-- (){
    //            point--;
    //            if (point < 0)
    //                point += buffer_size;
    //            return *this;
    //        }
    //        bool operator < (const ui _point) const {
    //            return point < _point;
    //        }
    //    };
    
    typedef ElementType* iterator;
    typedef const iterator const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef const std::reverse_iterator<const_iterator> const_reverse_iterator;
    
    ui buffer_size;
    ui _begin, _end;
    ElementType *buffer;
    
    
    
    ElementType *getNewBuffer(ui new_buffer_size) {
        ElementType *tmp;
        ui n_begin, n_end;
        tmp = new ElementType[new_buffer_size];
        n_begin = new_buffer_size / 2 - (_end - _begin) / 2;
        n_end = n_begin + (_end - _begin);
        for (int i = n_begin; i < n_end; ++i) {
            tmp[i] = buffer[i - n_begin + _begin];
        }
        _begin = n_begin, _end = n_end, buffer_size = new_buffer_size;
        delete[] buffer;
        return tmp;
    }
    
    void rebuildPush(EOperationType operationType, ElementType element) {
        switch (operationType) {
            case PUSH_BACK:
                if (_end - _begin + 1 > buffer_size / 2)
                    buffer = getNewBuffer(buffer_size * 2);
                else
                    buffer = getNewBuffer(buffer_size);
                buffer[_end++] = element;
                break;
            case PUSH_FRONT:
                if (_end - _begin + 1 > buffer_size / 2)
                    buffer = getNewBuffer(buffer_size * 2);
                else
                    buffer = getNewBuffer(buffer_size);
                buffer[--_begin] = element;
                break;
            default:
                assert(false);
                break;
        }
    }
    
    void rebuildPop() {
        buffer = getNewBuffer(buffer_size / 2);
    }
    
    
public:
    
    MyDeque(): buffer_size(4), _begin(1), _end(1) {
        buffer = new ElementType[buffer_size];
    };
    
    void push_back(ElementType element) {
        if (_end < buffer_size) {
            buffer[_end] = element;
            _end++;
        } else {
            rebuildPush(PUSH_BACK, element);
        }
    }
    
    void push_front(ElementType element) {
        if (_begin > 0) {
            _begin--;
            buffer[_begin] = element;
        } else {
            rebuildPush(PUSH_FRONT, element);
        }
    }
    
    void pop_back() {
        assert(_begin != _end);
        _end--;
        if (buffer_size > 4 && _end - _begin == buffer_size / 4) {
            rebuildPop();
        }
    }
    
    void pop_front() {
        assert(_begin != _end);
        _begin++;
        if (buffer_size > 4 && _end - _begin == buffer_size / 4) {
            rebuildPop();
        }
    }
    
    iterator begin() {
        return buffer + _begin;
    }
    
    const_iterator begin() const {
        return buffer + _begin;
    }
    
    const_iterator cbegin() {
        return buffer + _begin;
    }
    
    reverse_iterator rbegin() {
        return reverse_iterator(buffer + _end);
    }
    
    const_reverse_iterator rbegin() const {
        return const_reverse_iterator(buffer + _end);
    }
    
    const_reverse_iterator crbegin() {
        return const_reverse_iterator(buffer + _end);
    }
    
    iterator end() {
        return buffer + _end;
    }
    
    const_iterator end() const {
        return buffer + _end;
    }
    
    const_iterator cend() {
        return buffer + _end;
    }
    
    reverse_iterator rend() {
        return reverse_iterator(buffer + _begin);
    }
    
    const_reverse_iterator rend() const {
        return const_reverse_iterator(buffer + _begin);
    }
    
    const_reverse_iterator crend() {
        return const_reverse_iterator(buffer + _begin);
    }
    
    ElementType &front() {
        return buffer[_begin];
    }
    
    ElementType &front() const {
        return buffer[_begin];
    }
    
    ElementType &back() {
        return buffer[_end - 1];
    }
    
    ElementType &back() const {
        return buffer[_end - 1];
    }
    
    ui size() const {
        return _end - _begin;
    }
    
    bool empty() const {
        return _begin == _end;
    }
    
    void print() {
        for (int i = 0; i < buffer_size; ++i) {
            if (_begin <= i && i < _end) {
                std::cout << buffer[i] << " ";
            } else {
                std::cout << "# ";
            }
        }
    }
    
    ElementType & operator[] (ui index) {
        return buffer[_begin + index];
    }
    
    const ElementType & operator[] (ui index) const {
        return buffer[_begin + index];
    }
};



#endif
